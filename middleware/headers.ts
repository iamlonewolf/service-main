import { AUTH_TOKEN_KEY } from "./../routes/identity";
export const HEADER_KEY_COUNT = "count";
export default function (req, res, next) {
  res.header(
    "Access-Control-Expose-Headers",
    [AUTH_TOKEN_KEY, HEADER_KEY_COUNT].join(", ")
  );
  next();
}
