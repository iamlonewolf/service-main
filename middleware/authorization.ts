import { STATUS_CODES } from "http";
import { constants } from "http2";
import { verify } from "jsonwebtoken";
const { HTTP_STATUS_UNAUTHORIZED, HTTP_STATUS_BAD_REQUEST } = constants;
import config from "../config/config";
import { AUTH_TOKEN_KEY } from "./../routes/identity";
export default function (req, res, next) {
  const token = req.header(AUTH_TOKEN_KEY);
  if (!token && !config.disableAuth) {
    return res.status(HTTP_STATUS_UNAUTHORIZED).json({
      message: STATUS_CODES[HTTP_STATUS_UNAUTHORIZED],
    });
  }

  try {
    if (!config.disableAuth) {
      const decoded = verify(token, config?.jwtPrivateKey);
      req.account = decoded;
    }
    next();
  } catch (error) {
    console.log("authorization error:", error);
    return res
      .status(HTTP_STATUS_BAD_REQUEST)
      .send(STATUS_CODES[HTTP_STATUS_BAD_REQUEST]);
  }
}
