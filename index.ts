/**
 * Required External Modules
 */
import express from "express";
import { createServer } from "http";
import { Server } from "socket.io";
import cors from "cors";
import helmet from "helmet";
import httpProxy from "http-proxy";

import config from "./config/config";
import initDb from "./db";
import routes from "./routes/index";
import initializeWebsocket from "./websocket/index";
const app = express();
const server = createServer(app);
const io = new Server(server, {
  cors: {
    // origin: [
    //   "http://localhost:3000",
    //   "https://localhost:3000",
    //   "http://192.168.1.6:3000",
    // ],
    origin: "*",
  },
  // transports: ["polling"],
});

app.set("socketio", io);

/**
 *  App Configuration
 */
if (!config.serverPort) {
  process.exit(1);
}
// dotenv.config();

/***
 * Routes
 */
app.use(helmet());
app.use(
  cors({
    origin: "*",
  })
);

//  app.use(function(req,res,next){setTimeout(next, 2000)});
app.use(express.json());
app.use("/api", routes);
initializeWebsocket(io);

/**
 * Server Activation
 */
server.listen(config.serverPort, () => {
  console.log(`listening to port ${config.serverPort}`);
});

// initialize database
initDb();
