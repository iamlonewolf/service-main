import config from "./config/config";
import { connect, connection } from "mongoose";
import { isUndefined } from "lodash";

export default function () {
  connection.on("connecting", () => {
    console.log("connecting to database..");
  });

  connection.on("open", () => {
    console.log("successfully connected to database.");
  });
  connection.on("error", () => {
    console.log(
      "failed connecting to database. Please check your configuration settings."
    );
  });

  console.log("connecting to: ", config.dbConnectionString);
  console.log("user: ", config.dbUser);
  if (isUndefined(config.dbConnectionString)) {
    return;
  }

  connect(encodeURI(config.dbConnectionString), {
    auth: {
      user: config.dbUser,
      password: config.dbPassword,
    },
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  });
}
