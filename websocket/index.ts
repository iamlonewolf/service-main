import { Server } from "socket.io";
import adminWebsocket from "./admin";

export default function (io: Server) {
  adminWebsocket(io);
}
