import { Server } from "socket.io";
import {
  AccountConnectionMonitor,
  buildAccountConnectionList,
} from "./../models/interfaces/Account";
export const NS_ADMIN = "/admin";
export const EVENT_ADMIN_LIST = "list";
export default function (io: Server) {
  const admin = io.of(NS_ADMIN);
  admin.on("connection", (socket) => {
    console.log(io.engine.clientsCount);
    console.log(`clients connected ${socket?.id}`);
    socket.emit(
      EVENT_ADMIN_LIST,
      buildAccountConnectionList(AccountConnectionMonitor.getList())
    );
  });

  io.engine.on("connection_error", (err) => {
    console.log(err.req); // the request object
    console.log(err.code); // the error code, for example 1
    console.log(err.message); // the error message, for example "Session ID unknown"
    console.log(err.context); // some additional error context
  });
}
