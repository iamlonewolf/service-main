import { Router } from "express";
import headers from "../middleware/headers";
import identityRoutes from "./identity";
import accountRoutes from "./account";
import connectionStateRoutes from "./connectionState";
const router = Router();

router.use(headers);
router.use("/identity", identityRoutes);
router.use("/account", accountRoutes);
router.use("/connectionState", connectionStateRoutes);

router.get("/", (req, res) => {
  res.json({
    app: "AFKTracker API",
  });
});

export default router;
