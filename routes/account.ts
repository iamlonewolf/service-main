import { Router } from "express";
import { constants } from "http2";
import { isEmpty } from "lodash";
import authorization from "../middleware/authorization";
import { getAccountLiteById, getAccount } from "../models/schemas/Account";
import { addState } from "../models/schemas/AccountConnectionState";
import {
  CONNECTION_STATE_ACTIVE,
  CONNECTION_STATE_AWAY,
  CONNECTION_STATE_BUSY,
  CONNECTION_STATE_INACTIVE,
  CONNECTION_STATE_OFF,
  getConnectionStateByLabel,
} from "../models/schemas/ConnectionState";
import { EVENT_ADMIN_LIST } from "../websocket/admin";
import { NS_ADMIN } from "./../websocket/admin";
import {
  AccountConnectionMonitor,
  AccountConnectionItem,
  buildAccountConnectionList,
} from "./../models/interfaces/Account";

const router = Router();
const { HTTP_STATUS_BAD_REQUEST, HTTP_STATUS_CREATED, HTTP_STATUS_NOT_FOUND } =
  constants;

const setConnectionState = async (req, res, status) => {
  const admin = req.app.get("socketio").of(NS_ADMIN);
  const accountId = req?.account?._id;
  try {
    const connectionState = await getConnectionStateByLabel(status);
    const account = await getAccountLiteById(accountId);
    const item = new AccountConnectionItem();
    item.account = account;
    item.connectionState = connectionState;
    item.dateAdded = new Date().toISOString();
    AccountConnectionMonitor.addAccount(item);

    admin.emit(
      EVENT_ADMIN_LIST,
      buildAccountConnectionList(AccountConnectionMonitor.getList())
    );

    await addState(req?.account?._id, {
      deviceId: req?.body?.deviceId,
      connectionState: connectionState?._id,
    });
    res.status(HTTP_STATUS_CREATED).json({
      message: "ok",
      accountId: req?.account?._id,
    });
  } catch (error) {
    res.status(HTTP_STATUS_BAD_REQUEST).send({
      message: "error",
    });
  }
};

router.get("/:accountId", [authorization], async (req, res) => {
  try {
    const result = await getAccount(req?.params?.accountId);
    if (isEmpty(result)) {
      throw new Error("Not found");
    }
    res.json(result);
  } catch (error) {
    res.status(HTTP_STATUS_NOT_FOUND).send({
      message: "Not found",
    });
  }
});

router.post("/connectionState/active", [authorization], async (req, res) => {
  setConnectionState(req, res, CONNECTION_STATE_ACTIVE);
});

router.post("/connectionState/inactive", [authorization], async (req, res) => {
  setConnectionState(req, res, CONNECTION_STATE_INACTIVE);
});

router.post("/connectionState/away", [authorization], async (req, res) => {
  setConnectionState(req, res, CONNECTION_STATE_AWAY);
});

router.post("/connectionState/busy", [authorization], async (req, res) => {
  setConnectionState(req, res, CONNECTION_STATE_BUSY);
});

router.post("/connectionState/off", [authorization], async (req, res) => {
  setConnectionState(req, res, CONNECTION_STATE_OFF);
});

export default router;
