import { Router } from "express";
import { constants } from "http2";

const {
  HTTP_STATUS_BAD_REQUEST,
  HTTP_STATUS_CREATED,
  HTTP_STATUS_INTERNAL_SERVER_ERROR,
} = constants;
import { compare } from "bcrypt";
import { isEmpty, head, omit } from "lodash";
import {
  createUser,
  getAccountByEmail,
  generateAuthToken,
  validateAccount,
  validateLogin,
} from "../models/schemas/Account";
import { errorExtractor } from "../utilities/response";
const router = Router();

export const AUTH_TOKEN_KEY = "authorization";
router.post("/", async (req, res) => {
  const { firstName, lastName, email, position, department, password } =
    req.body;
  try {
    const { error } = validateAccount(req?.body);
    if (!isEmpty(error?.details)) {
      return res
        .status(HTTP_STATUS_BAD_REQUEST)
        .send(head(error?.details).message);
    }
    const user = await getAccountByEmail(email);
    if (!isEmpty(user)) {
      return res.status(HTTP_STATUS_BAD_REQUEST).send("Email already exists");
    }
    const result = await createUser({
      firstName,
      lastName,
      position,
      department,
      email,
      password,
    });
    res
      .status(HTTP_STATUS_CREATED)
      .header("Authorization", generateAuthToken(result))
      .send(omit(result, ["password"]));
  } catch (error) {
    if (error?.errors) {
      return res
        .status(HTTP_STATUS_BAD_REQUEST)
        .send(
          errorExtractor(error?.errors, [
            "firstName",
            "lastName",
            "password",
            "email",
          ])
        );
    }
    res.status(HTTP_STATUS_INTERNAL_SERVER_ERROR).send(error);
  }
});

router.post("/login", async (req, res) => {
  console.log(req?.params);
  console.log(req?.body);
  console.log(req?.query);
  const { email, password } = req?.body;
  try {
    console.log("INITIALIZE LOGIN");
    const { error } = validateLogin(email, password);
    if (!isEmpty(error?.details)) {
      return res
        .status(HTTP_STATUS_BAD_REQUEST)
        .json(head(error?.details).message);
    }
    const account = await getAccountByEmail(email);
    if (isEmpty(account)) {
      return res
        .status(HTTP_STATUS_BAD_REQUEST)
        .json("Invalid email or password");
    }
    const validPassword = await compare(password, account?.password);
    if (!validPassword) {
      return res
        .status(HTTP_STATUS_BAD_REQUEST)
        .json("Invalid email or password.");
    }
    res.header(AUTH_TOKEN_KEY, generateAuthToken(account)).json("ok");
  } catch (error) {
    res.status(HTTP_STATUS_INTERNAL_SERVER_ERROR).json(error);
  }
});

export default router;
