import { Router } from "express";
import { isEmpty } from "lodash";
import {
  addConnectionState,
  CONNECTION_STATE_ACTIVE,
  CONNECTION_STATE_INACTIVE,
  CONNECTION_STATE_AWAY,
  CONNECTION_STATE_OFF,
  getConnectionStateByLabel,
  CONNECTION_STATE_BUSY,
} from "../models/schemas/ConnectionState";

const router = Router();

router.get("/seed", async (req, res) => {
  const connectionStateActiveParams = {
    label: CONNECTION_STATE_ACTIVE,
  };
  const connectionStateInactiveParams = {
    label: CONNECTION_STATE_INACTIVE,
  };
  const connectionStateAwayParams = {
    label: CONNECTION_STATE_AWAY,
  };
  const connectionStateBusyParams = {
    label: CONNECTION_STATE_BUSY,
  };
  const connectionStateOffParams = {
    label: CONNECTION_STATE_OFF,
  };

  const resultActive = await getConnectionStateByLabel(CONNECTION_STATE_ACTIVE);
  if (isEmpty(resultActive)) {
    await addConnectionState(connectionStateActiveParams);
    console.log("active state saved.");
  }

  const resultInactive = await getConnectionStateByLabel(
    CONNECTION_STATE_INACTIVE
  );
  if (isEmpty(resultInactive)) {
    await addConnectionState(connectionStateInactiveParams);
    console.log("inactive state saved.");
  }

  const resultAway = await getConnectionStateByLabel(CONNECTION_STATE_AWAY);
  if (isEmpty(resultAway)) {
    await addConnectionState(connectionStateAwayParams);
    console.log("away state saved.");
  }

  const resultBusy = await getConnectionStateByLabel(CONNECTION_STATE_BUSY);
  if (isEmpty(resultBusy)) {
    await addConnectionState(connectionStateBusyParams);
    console.log("busy state saved.");
  }

  const resultOff = await getConnectionStateByLabel(CONNECTION_STATE_OFF);
  if (isEmpty(resultOff)) {
    await addConnectionState(connectionStateOffParams);
    console.log("off state saved.");
  }

  res.send({
    message: "ok",
  });
});

export default router;
