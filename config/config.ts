const config = {
  serverPort: process.env.SERVER_PORT || 3003,
  dbConnectionString: process.env.DB_CONNECTION_STRING,
  passwordSalt: 10,
  jwtPrivateKey: process.env.JWT_PRIVATE_KEY,
  disableAuth: false,
  dbUser: process.env.DB_USER,
  dbPassword: process.env.DB_PASSWORD,
};

export default config;
