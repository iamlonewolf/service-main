import { Schema, model, models } from "mongoose";
import { ConnectionState } from "../interfaces/ConnectionState";

const DEFAULT_MAX_LENGTH = 255;
const DEFAULT_MIN_LENGTH = 5;

export const CONNECTION_STATE_ACTIVE = "active";
export const CONNECTION_STATE_INACTIVE = "inactive";
export const CONNECTION_STATE_AWAY = "away";
export const CONNECTION_STATE_BUSY = "busy";
export const CONNECTION_STATE_OFF = "off";

export const connectionStateSchema = new Schema<ConnectionState>(
  {
    label: {
      type: String,
      required: true,
    },
    dateCreated: {
      type: String,
    },
  },
  {
    timestamps: {
      createdAt: "createdAt",
    },
    toJSON: { virtuals: true },
  }
);

export const ConnectionStateModel =
  models?.ConnectionState ??
  model<ConnectionState>("ConnectionState", connectionStateSchema);

export async function addConnectionState(
  connectionState: ConnectionState
): Promise<ConnectionState> {
  const model = new ConnectionStateModel(connectionState);
  return await model.save();
}

export async function getConnectionStateByLabel(
  label: string
): Promise<ConnectionState> {
  return await ConnectionStateModel.findOne({
    label,
  });
}
