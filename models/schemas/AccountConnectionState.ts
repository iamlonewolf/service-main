import { Schema, model, models, FilterQuery } from "mongoose";
import { AccountConnectionState } from "../interfaces/AccountConnectionState";
import { AccountModel } from "./Account";

const DEFAULT_MAX_LENGTH = 255;
const DEFAULT_MIN_LENGTH = 5;

export const accountConnectionStateSchema = new Schema<AccountConnectionState>(
  {
    account: {
      type: Schema.Types.ObjectId,
      ref: "Account",
    },
    deviceId: {
      type: String,
      required: true,
    },
    connectionState: {
      type: Schema.Types.ObjectId,
      ref: "ConnectionState",
    },
    dateCreated: {
      type: String,
    },
  },
  {
    timestamps: {
      createdAt: "createdAt",
    },
    toJSON: { virtuals: true },
  }
);

export const AccountConnectionStateModel =
  models?.AccountConnectionState ??
  model<AccountConnectionState>(
    "AccountConnectionState",
    accountConnectionStateSchema
  );

export async function createState(
  accountConnectionstate: AccountConnectionState
): Promise<AccountConnectionState> {
  const accountConnectionStateModel = new AccountConnectionStateModel(
    accountConnectionstate
  );
  return await accountConnectionStateModel.save();
}

export async function addState(
  accountId,
  accountConnectionstate: AccountConnectionState
): Promise<AccountConnectionState> {
  const query: FilterQuery<any> = { _id: accountId };
  const account = await AccountModel.findOne(query);
  const accountConnectionStateModel = new AccountConnectionStateModel(
    accountConnectionstate
  );
  accountConnectionStateModel.account = accountId;
  await accountConnectionStateModel.save();

  if (!account?.connectionStates) {
    account.connectionStates = [];
  }
  account.connectionStates.push(accountConnectionStateModel);
  return await account.save();
}
