import { Schema, model, models } from "mongoose";
import { genSalt, hash } from "bcrypt";
import { sign } from "jsonwebtoken";
import config from "../../config/config";
import { Account } from "../interfaces/Account";
import Joi from "joi";

const DEFAULT_MAX_LENGTH = 255;
const DEFAULT_MIN_LENGTH = 5;

export const accountSchema = new Schema<Account>(
  {
    firstName: {
      type: String,
      required: true,
      max: DEFAULT_MAX_LENGTH,
    },
    lastName: {
      type: String,
      required: true,
      max: DEFAULT_MAX_LENGTH,
    },
    position: {
      type: String,
      max: DEFAULT_MAX_LENGTH,
    },
    department: {
      type: String,
      max: DEFAULT_MAX_LENGTH,
    },
    email: {
      type: String,
      required: true,
      min: DEFAULT_MIN_LENGTH,
      max: DEFAULT_MAX_LENGTH,
    },
    password: {
      type: String,
      required: true,
      min: DEFAULT_MIN_LENGTH,
      max: DEFAULT_MAX_LENGTH,
      select: false,
    },
    connectionStates: [
      {
        type: Schema.Types.ObjectId,
        ref: "AccountConnectionState",
      },
    ],
    dateCreated: {
      type: String,
    },
  },
  {
    timestamps: {
      createdAt: "createdAt",
    },
    toJSON: { virtuals: true },
  }
);

export const generateAuthToken = function ({
  _id,
  firstName,
  lastName,
  email,
}: Account) {
  return sign(
    {
      _id,
      firstName,
      lastName,
      email,
    },
    config.jwtPrivateKey
  );
};

export const AccountModel =
  models.Account ?? model<Account>("Account", accountSchema);

export async function createUser(account: Account): Promise<Account> {
  await AccountModel.init();
  const accountModel = new AccountModel(account);
  const salt = await genSalt(config.passwordSalt);
  accountModel.password = await hash(account?.password, salt);
  return await accountModel.save();
}

export async function getAccountByEmail(email: string): Promise<Account> {
  return await AccountModel.findOne({ email }).select(
    "password firstName lastName"
  );
}

export async function getAccount(id: string): Promise<Account> {
  return await AccountModel.findById(id).select("-__v -createdAt -updatedAt");
}

export async function getAccountLiteById(id: string): Promise<Account> {
  return await AccountModel.findById(id).select(
    "-__v -createdAt -updatedAt -connectionStates"
  );
}

export const validateAccount = (account: Account) => {
  const schema = Joi.object({
    firstName: Joi.string().max(DEFAULT_MAX_LENGTH).required(),
    lastName: Joi.string().max(DEFAULT_MAX_LENGTH).required(),
    position: Joi.string().max(DEFAULT_MAX_LENGTH),
    department: Joi.string().max(DEFAULT_MAX_LENGTH),
    email: Joi.string()
      .min(DEFAULT_MIN_LENGTH)
      .max(DEFAULT_MAX_LENGTH)
      .required()
      .email({ tlds: { allow: false } }),
    password: Joi.string()
      .min(DEFAULT_MIN_LENGTH)
      .max(DEFAULT_MAX_LENGTH)
      .required(),
  });
  return schema.validate(account);
};

export const validateLogin = (email: string, password: string) => {
  const schema = Joi.object({
    email: Joi.string()
      .min(DEFAULT_MIN_LENGTH)
      .max(DEFAULT_MAX_LENGTH)
      .required()
      .email({ tlds: { allow: false } }),
    password: Joi.string()
      .min(DEFAULT_MIN_LENGTH)
      .max(DEFAULT_MAX_LENGTH)
      .required(),
  });
  return schema.validate({ email, password });
};
