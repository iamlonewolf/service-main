import { Schema, model, models } from "mongoose";
import { AccountAttendance } from "../interfaces/AccountAttendance";

const DEFAULT_MAX_LENGTH = 255;
const DEFAULT_MIN_LENGTH = 5;

export const accountAttendanceSchema = new Schema<AccountAttendance>(
  {
    accountId: {
      type: String,
      required: true,
    },
    deviceId: {
      type: String,
      required: true,
    },
    timeIn: {
      type: String,
    },
    timeOut: {
      type: String,
    },
    dateCreated: {
      type: String,
    },
  },
  {
    timestamps: {
      createdAt: "createdAt",
    },
    toJSON: { virtuals: true },
  }
);

export const AccountAttendanceModel = models?.AccountAttendance ?? 
  model("AccountAttendance", accountAttendanceSchema);



