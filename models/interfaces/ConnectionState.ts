export interface ConnectionState {
  _id?: string;
  label?: string;
  dateCreated?: string;
}
