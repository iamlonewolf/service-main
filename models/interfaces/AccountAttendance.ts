export interface AccountAttendance {
  _id?: number;
  accountId?: string;
  deviceId?: string;
  timeIn?: string;
  timeOut?: string;
  dateCreated?: string;
}
