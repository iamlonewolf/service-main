export interface AccountConnectionState {
  _id?: string;
  account?: string;
  deviceId?: string;
  connectionState?: string;
  lastDegreeAngle?: number;
  dateCreated?: string;
}
