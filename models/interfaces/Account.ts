import { filter, orderBy, uniqBy } from "lodash";
import { CONNECTION_STATE_OFF } from "../schemas/ConnectionState";
import { AccountConnectionState } from "./AccountConnectionState";
import { ConnectionState } from "./ConnectionState";
export interface Account {
  _id?: number;
  firstName?: string;
  lastName?: string;
  position?: string;
  department?: string;
  email?: string;
  password?: string;
  connectionStates?: AccountConnectionState[];
  dateCreated?: string;
  dateUpdated?: string;
}

export class AccountConnectionItem {
  connectionState: ConnectionState;
  account: Account;
  dateAdded: string;
}

export class AccountConnectionMonitor {
  private static list: AccountConnectionItem[] = [];

  static addAccount(item: AccountConnectionItem): void {
    AccountConnectionMonitor.list.push(item);
  }

  static setList(items: AccountConnectionItem[]): void {
    AccountConnectionMonitor.list = items;
  }

  static getList(): AccountConnectionItem[] | undefined {
    return AccountConnectionMonitor.list;
  }
}

export const buildAccountConnectionList = (
  items: AccountConnectionItem[]
): AccountConnectionItem[] | undefined => {
  return filter(
    uniqBy(orderBy(items, ["dateAdded"], ["desc"]), "account.email"),
    (item) => item?.connectionState?.label != CONNECTION_STATE_OFF
  );
};
