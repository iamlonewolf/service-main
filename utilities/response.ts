import { mapValues, pick } from "lodash";
import { ValidationError } from "joi";
export const errorExtractor = (errors: ValidationError, keys: string[]) => {
  return mapValues(pick(errors, keys), "message");
};
